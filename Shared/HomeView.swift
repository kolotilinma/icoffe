//
//  HomeView.swift
//  Shared
//
//  Created by Михаил on 27.06.2020.
//

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var drinkListener = DrinkListener()
    
    var categories: [String: [Drink]] {
        .init(
            grouping: drinkListener.drinks,
            by: {$0.category.rawValue}
        )
    }
    
    var body: some View {
        
        NavigationView {
            
            List(categories.keys.sorted(), id: \String.self) { key in
                DrinkRow(categoryName: "\(key) Drink".uppercased(),
                         drinks: self.categories[key]!)
                    .frame(height: 320)
                    .padding(0)
            }
            .padding(-20)
                .navigationTitle(Text("iCoffee"))
                .navigationBarItems(leading: Button(action: {
                    print("Log out button pressed")
                }, label: {
                    Text("LogOut")
                }), trailing: Button(action: {
                    print("Busket button pressed")
                }, label: {
                    Image(systemName: "cart")
                }))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .previewDevice("iPhone 11")
    }
}


