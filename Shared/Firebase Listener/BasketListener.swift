//
//  BasketListener.swift
//  iCoffe
//
//  Created by Михаил on 12.07.2020.
//

import Foundation
import Firebase

class BasketListener: ObservableObject {
    
    @Published var orderBasket: OrderBasket!
    
    init() {
        dowloadBasket()
    }
    
    func dowloadBasket() {
        FirebaseReference(.Basket).whereField(kOWNERID, isEqualTo: "123456").addSnapshotListener { (snapshot, error) in
            guard let snapshot = snapshot else { return }
            if !snapshot.isEmpty {
                let basketData = snapshot.documents.first!.data()
                
                getDrinksFromFirestore(withIds: basketData[kDRINKIDS] as? [String] ?? [] ) { (allDrinks) in
                    let basket = OrderBasket()
                    basket.ornerId = basketData[kOWNERID] as? String
                    basket.id = basketData[kID] as? String
                    basket.items = allDrinks
                    self.orderBasket = basket
                }
            }
        }
    }
        
}

func getDrinksFromFirestore(withIds: [String], completion: @escaping (_ drinkArray:  [Drink]) -> Void ) {
    var count = 0
    var drinkArray: [Drink] = []
    
    if withIds.count == 0 {
        completion(drinkArray)
        return
    }
    
    for drinkId in withIds {
        FirebaseReference(.Menu).whereField(kID, isEqualTo: drinkId).getDocuments { (snapshot, error) in
            guard let snapshot = snapshot else { return }
            if !snapshot.isEmpty {
                let drinkData = snapshot.documents.first!
                guard let drink = Drink(with: drinkData) else { return }
                drinkArray.append(drink)
                count += 1
            } else {
                print("have no drink")
                completion(drinkArray)
            }
            if count == withIds.count {
                completion(drinkArray)
            }
        }

    }
    
}
