//
//  DrinkListener.swift
//  iCoffe
//
//  Created by Михаил on 09.07.2020.
//

import Foundation
import FirebaseFirestore


class DrinkListener: ObservableObject {
    
    @Published var drinks: [Drink] = []
    
    init() {
        downloadDrinks()
    }
    
    func downloadDrinks() {
        FirebaseReference(.Menu).getDocuments { (snapshot, error) in
            
            guard let snapshot = snapshot else { return }
            if !snapshot.isEmpty {
                self.drinks = DrinkListener.drinkFromDictionary(snapshot)
            }
        }
    }
    
    static func drinkFromDictionary(_ snapshot: QuerySnapshot) -> [Drink] {
        var allDrinks: [Drink] = []
        
        for drinkData in snapshot.documents {
            let drinkItem = Drink(with: drinkData)
            allDrinks.append(drinkItem!)
        }
        return allDrinks
    }
    
}
