//
//  Constants.swift
//  iCoffe
//
//  Created by Михаил on 27.06.2020.
//

import Foundation

public let userDefaults = UserDefaults.standard

//Drink
public let kID = "id"
public let kNAME = "name"
public let kPRICE = "price"
public let kDESCRIPTION = "description"
public let kCATEGORY = "category"
public let kIMAGENAME = "imageName"

//Order
public let kDRINKIDS = "drinkIds"
public let kOWNERID = "ounerId"
public let kCUSTOMERID = "customerId"
public let kAMOUNT = "amount"

//FUser
public let kEMAIL = "email"
public let kFIRSTNAME = "firstname"
public let kLASTNAME = "lastname"
public let kFULLNAME = "fullname"
public let kCURRENTUSER = "currentUser"
public let kFULLADDRESS = "fullAddress"
public let kPHONENUMBER = "phoneNumber"
public let kONBOARD = "onBoard"
