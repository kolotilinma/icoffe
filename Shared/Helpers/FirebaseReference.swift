//
//  FirebaseReference.swift
//  iCoffe
//
//  Created by Михаил on 27.06.2020.
//

import Foundation
import FirebaseFirestore

enum FCollectionReferece: String {
    case User
    case Menu
    case Order
    case Basket
}

func FirebaseReference(_ collectionReference: FCollectionReferece) -> CollectionReference {
    return Firestore.firestore().collection(collectionReference.rawValue)
}
