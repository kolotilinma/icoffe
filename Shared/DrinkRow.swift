//
//  DrinkRow.swift
//  iCoffe
//
//  Created by Михаил on 09.07.2020.
//

import SwiftUI

struct DrinkRow: View {
    
    var categoryName: String
    var drinks: [Drink]
    
    var body: some View {
        VStack(alignment: .leading, content: {
            Text(self.categoryName)
                .font(.title)
            ScrollView(.horizontal, showsIndicators: false, content: {
                HStack(content: {
                    ForEach(self.drinks) { drink in
                        NavigationLink(
                            destination: DrinkDetail(drink: drink),
                            label: {
                                DrinkItem(drink: drink)
                                    .frame(width: 300)
                                    .padding(.trailing, 30)
                            })
                    }
                })
            })
        })
    }
}

struct DrinkRow_Previews: PreviewProvider {
    static var previews: some View {
        DrinkRow(categoryName: "HOT DRINK", drinks: drinkData)
    }
}
