//
//  DrinkDetail.swift
//  iCoffe
//
//  Created by Михаил on 10.07.2020.
//

import SwiftUI

struct DrinkDetail: View {
    
    @State private var showingAlert = false
    
    var drink: Drink
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false, content: {
            ZStack(alignment: .bottom, content: {
                Image(drink.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Rectangle()
                    .frame(height: 80)
                    .foregroundColor(.black)
                    .opacity(0.35)
                    .blur(radius: 10)
                HStack(content: {
                    VStack(alignment: .leading, spacing: 10, content: {
                        Text(drink.name)
                            .foregroundColor(.white)
                            .font(.largeTitle)
                    })
                    .padding([.leading, .bottom])
                    Spacer()
                })
            })
            .listRowInsets(EdgeInsets())
            Text(drink.description)
                .foregroundColor(.primary)
                .font(.body)
                .lineLimit(5)
                .padding()
            HStack(content: {
                Spacer()
                OrderButton(showAlert: $showingAlert, drink: self.drink)
                Spacer()
            })
            .padding(.top, 50)
        })
        .edgesIgnoringSafeArea(.top)
//        .navigationBarHidden(true)
        .alert(isPresented: $showingAlert) {
            Alert(title: Text("Added to basket!"), dismissButton: .default(Text("OK")))
        }
    }
}

struct DrinkDetail_Previews: PreviewProvider {
    static var previews: some View {
        DrinkDetail(drink: drinkData[0])
    }
}


struct OrderButton: View {
    @ObservedObject var basketListener = BasketListener()
    @Binding var showAlert: Bool
    var drink: Drink
    
    var body: some View {
        Button(action: {
            showAlert.toggle()
            addItemToBasket()
            print("Add to basket, \(drink.name)")
        },
               label: {
            Text("Add to basket")
        })
        .frame(width: 200, height: 50)
        .foregroundColor(.white)
        .font(.headline)
        .background(Color.blue)
        .cornerRadius(10)
        
    }
    
    private func addItemToBasket() {
        var orderBasket: OrderBasket!
        
        if basketListener.orderBasket != nil {
            orderBasket = basketListener.orderBasket
        } else {
            orderBasket = OrderBasket()
            orderBasket.ornerId = "123456"
            orderBasket.id = UUID().uuidString
        }
        orderBasket.add(drink)
        orderBasket.saveBasketToFirestore()
    }
    
}
